﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseHealthVisual : MonoBehaviour
{
    Health health;
    float StartingHealth;
    public GameObject Visual;
    // Start is called before the first frame update
    void Start()
    {
        health = GetComponent<Health>();
        StartingHealth = health.health;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = Visual.transform.position;
        pos.y = ((health.health/StartingHealth) * 4) - 1;
        Visual.transform.position = pos;
    }
}

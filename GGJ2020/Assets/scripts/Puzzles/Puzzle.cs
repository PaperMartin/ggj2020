﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Puzzle : ScriptableObject
{
    public List<GameObject> PuzzlePiecePrefabs = new List<GameObject>();
    public List<Vector3> PiecesPosition;
    public GameObject PuzzleRobotPrefab;
    public GameObject RobotUnitPrefab;
    public float AttackDPS = 2;
    public float Health = 10;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleSpawner : MonoBehaviour
{
    PuzzleManager manager;
    public List<Puzzle> PuzzlePool = new List<Puzzle>();
    // Start is called before the first frame update
    void Start()
    {
        manager = GetComponentInParent<PuzzleManager>();
    }

    public void Interact(){
        manager.CurrentPuzzle = PuzzlePool[Random.Range(0,PuzzlePool.Count)];
        manager.SpawnPuzzle();
    }
}

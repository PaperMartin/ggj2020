﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PuzzlePieceInteraction : MonoBehaviour
{
    GameObject HoveredPuzzlePiece;
    GameObject HeldPuzzlePiece;
    GameObject HoveredButton;

    private void OnTriggerStay2D(Collider2D other) {
        if (other.transform.root.GetComponent<PuzzlePiece>() != null){
            Debug.Log("Puzzle Piece Detected");
            HoveredPuzzlePiece = other.transform.root.gameObject;
        }
        else if (other.GetComponent<PuzzleSpawner>() != null){
            HoveredButton = other.gameObject;
        }
    }
    private void OnTriggerExit2D(Collider2D other) {
        if (other.transform.root.gameObject == HoveredPuzzlePiece){
            HoveredPuzzlePiece = null;
        }
        if (other.gameObject == HoveredButton){
            HoveredButton = null;
        }
    }

    public void OnInteract(InputAction.CallbackContext context){
        if (context.phase != InputActionPhase.Started){
            return;
        }
        if (HeldPuzzlePiece != null){
            HeldPuzzlePiece.transform.parent = null;
            HeldPuzzlePiece.GetComponent<PuzzlePiece>().IsHeld = false;
            HeldPuzzlePiece = null;
            return;
        }
        if (HoveredPuzzlePiece != null){
            HoveredPuzzlePiece.transform.parent = transform;
            HoveredPuzzlePiece.transform.localPosition = Vector3.zero;
            HeldPuzzlePiece = HoveredPuzzlePiece;
            HeldPuzzlePiece.GetComponent<PuzzlePiece>().IsHeld = true;
            return; 
        }
        if(HoveredButton != null){
            HoveredButton.GetComponent<PuzzleSpawner>().Interact();
        }
    }
}

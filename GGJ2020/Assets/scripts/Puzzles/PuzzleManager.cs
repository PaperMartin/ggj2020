﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : MonoBehaviour
{
    public Puzzle CurrentPuzzle;
    List<PuzzlePiece> PuzzlePieces = new List<PuzzlePiece>();
    public int Player;
    List<Vector3> PiecesPosition;
    public float MaxDistanceFromPosition;
    public List<Transform> PieceSpawnPoints;
    GameObject RobotSilouhette;
    public Transform UnitSpawnPosition;
    public Transform SilouhettePosition;
    public List<GameObject> PuzzleButtons = new List<GameObject>(); 
    // Start is called before the first frame update
    void Start()
    {
        SpawnPuzzle();
    }

    public void SpawnPuzzle(){
        PuzzlePieces = new List<PuzzlePiece>();
        foreach(GameObject piece in CurrentPuzzle.PuzzlePiecePrefabs){
            GameObject piece2 = Instantiate(piece,PieceSpawnPoints[CurrentPuzzle.PuzzlePiecePrefabs.IndexOf(piece)].position,Quaternion.identity);
            PuzzlePieces.Add(piece2.GetComponent<PuzzlePiece>());
        }
        PiecesPosition = CurrentPuzzle.PiecesPosition;
        RobotSilouhette = Instantiate(CurrentPuzzle.PuzzleRobotPrefab,SilouhettePosition.position,Quaternion.identity);
        Color newcolor = Color.black;
        newcolor.a = 0.75f;
        RobotSilouhette.GetComponentInChildren<SpriteRenderer>().color = newcolor;
    }

    private void Update() {
        if (CurrentPuzzle == null) {
            foreach (GameObject button in PuzzleButtons){
            button.SetActive(true);
        }
            return;
        }
        foreach (GameObject button in PuzzleButtons){
            button.SetActive(false);
        }
        bool PuzzleCompleted = true;
        foreach(PuzzlePiece piece in PuzzlePieces){
            if (piece.IsHeld){
                PuzzleCompleted = false;
            }
            else if (Vector3.Distance(piece.transform.position,PiecesPosition[piece.PieceIndex] + SilouhettePosition.position) >= MaxDistanceFromPosition){
                PuzzleCompleted = false;
            }
        }
        if (PuzzleCompleted){
            foreach(PuzzlePiece piece in PuzzlePieces){
                Destroy(piece.gameObject);
            }
            Destroy(RobotSilouhette);
            GameObject newUnit;
            for (int i = 0; i < 4; i++){
                newUnit = Instantiate(CurrentPuzzle.RobotUnitPrefab,UnitSpawnPosition.position,Quaternion.identity);
                newUnit.GetComponent<TeamMember>().Team = Player;
                newUnit.GetComponent<RobotUnit>().attackDPS = CurrentPuzzle.AttackDPS;
                newUnit.GetComponent<Health>().health = CurrentPuzzle.Health;
            }
            CurrentPuzzle = null;
        }
    }

    private void OnDrawGizmos() {
        if (PiecesPosition != null)
        foreach (Vector3 currentVector in PiecesPosition){
            Gizmos.DrawWireSphere(SilouhettePosition.position + currentVector,MaxDistanceFromPosition);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchSceneAfterTime : MonoBehaviour
{
    public string NextScene;
    public float TimeBeforeNextScene = 5f;
    float timer = 0;

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (TimeBeforeNextScene <= timer){
            SceneManager.LoadScene(NextScene);
        }
    }
}

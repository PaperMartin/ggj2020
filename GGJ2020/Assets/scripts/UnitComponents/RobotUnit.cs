﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RobotUnit : MonoBehaviour
{
    public GameObject Target;
    public Health TargetHealth;
    public TeamMember _Member;
    public bool IsBeingAttacked;
    public float attackdistance;
    public float attackDPS;
    NavMeshAgent agent;

    private void Start() {
        agent = GetComponent<NavMeshAgent>();
        _Member = GetComponent<TeamMember>();
    }

    private void Update() {
        Attack();
    }

    public void Seek(){
        IsBeingAttacked = false;
        TeamMember EnemyBase = null;
        foreach (TeamMember member in TeamMember.TeamMembers){
            if (member.IsPlayerBase){
                if (member.Team != _Member.Team)
                EnemyBase = member;
            }
            else {
                RobotUnit memberUnit = member.GetComponent<RobotUnit>();
                if (member.Team !=  _Member.Team){
                    if (!memberUnit.IsBeingAttacked){
                        Target = member.gameObject;
                        memberUnit.IsBeingAttacked = true;
                        memberUnit.Target = gameObject;
                        TargetHealth = member.GetComponent<Health>();
                        memberUnit.TargetHealth = GetComponent<Health>();
                    }
                }
            }
        }
        if (Target == null){
            Target = EnemyBase.gameObject;
            TargetHealth = EnemyBase.GetComponent<Health>();
        }
    }

    public void Attack(){
        if (Target == null){
            IsBeingAttacked = false;
            Seek();
        }
        if (Target.GetComponent<TeamMember>().IsPlayerBase){
            IsBeingAttacked = false;
            Seek();
        }
        if (Vector3.Distance(Target.transform.position,transform.position) >= attackdistance){
            agent.SetDestination(Target.transform.position);
            agent.isStopped = false;
        }
        else {
            agent.isStopped = true;
            TargetHealth.Damage(attackDPS * Time.deltaTime);
        }

    }
}
    

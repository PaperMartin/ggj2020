﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamMember : MonoBehaviour
{
    public static List<TeamMember> TeamMembers = new List<TeamMember>();
    public int Team;
    public bool IsPlayerBase;
    
    private void Start() {
        TeamMembers.Add(this);
    }

    private void OnDestroy() {
        TeamMembers.Remove(this);
    }
}

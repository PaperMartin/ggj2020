﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CursorSpriteSwitcher : MonoBehaviour
{
    PlayerInput input;
    public Sprite P1Sprite;
    public Sprite P2Sprite;
    // Start is called before the first frame update
    void Start()
    {
        input = GetComponent<PlayerInput>();
        if (input.playerIndex == 0){
            GetComponentInChildren<SpriteRenderer>().sprite = P1Sprite;
        }
        else GetComponentInChildren<SpriteRenderer>().sprite = P2Sprite;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerCursor : MonoBehaviour
{
    [System.Serializable]
    public struct CoordinateRestrictions{
        public float minX;
        public float maxX;
        public float minY;
        public float maxY;
    }
    public CoordinateRestrictions Player1Zone;
    public CoordinateRestrictions Player2Zone;
    PlayerInput inputs;
    PlayerInputManager inputManager;
    public Vector2 Movement;
    public float movespeed = 10f;
    // Start is called before the first frame update
    void Start()
    {
        inputs = GetComponent<PlayerInput>();
        inputManager = FindObjectOfType<PlayerInputManager>();
        Debug.Log(inputs.playerIndex);
    }

    public void Move(InputAction.CallbackContext context){
        
        Movement = context.ReadValue<Vector2>();
    }

    private void Update() {
        transform.localPosition += new Vector3(Movement.x,Movement.y,0) * movespeed * Time.deltaTime;
        Vector3 newPosition = transform.position;
        if (inputs.playerIndex == 0){
            newPosition.x = Mathf.Clamp(newPosition.x,Player1Zone.minX,Player1Zone.maxX);
            newPosition.y = Mathf.Clamp(newPosition.y,Player1Zone.minY,Player1Zone.maxY);
        }
        if (inputs.playerIndex == 1){
            newPosition.x = Mathf.Clamp(newPosition.x,Player2Zone.minX,Player2Zone.maxX);
            newPosition.y = Mathf.Clamp(newPosition.y,Player2Zone.minY,Player2Zone.maxY);
        }
        transform.position = newPosition;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class SwitchSceneOnFinish : MonoBehaviour
{
    public string P1WinScene;
    public string P2WinScene;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        List<TeamMember> members = FindObjectsOfType<TeamMember>().ToList();
        List<TeamMember> bases = new List<TeamMember>();
        foreach (TeamMember member in members){
            if (member.IsPlayerBase){
                bases.Add(member);
            }
        }
        if (bases.Count < 2){
            if (bases[0].Team == 0){
                SceneManager.LoadScene(P1WinScene);
            }
            else SceneManager.LoadScene(P2WinScene);
        }
    }
}
